import SpiffRuleDataWebDto from '../../src/SpiffRuleDataWebDto';
import  dummy from '../dummy';

describe('saveRuleData',
    ()=>{
    describe('constructor',
        () =>{
        it('throws if SpiffRuleId is null',
            () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffRuleDataWebDto(
                    null,
                    dummy.spiffId,
                    dummy.serialNumber,
                    dummy.description,
                    dummy.multipleRequired
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'spiffRuleId required');
        });
        it('sets spiffRuleId', () => {
            /*
             arrange
             */
            const expectedSpiffRuleId = dummy.spiffRuleId;

            /*
             act
             */
            const objectUnderTest =
                new SpiffRuleDataWebDto(
                    expectedSpiffRuleId,
                    dummy.spiffId,
                    dummy.serialNumber,
                    dummy.description,
                    dummy.multipleRequired
                );

            /*
             assert
             */
            const actualSpiffRuleId =
                objectUnderTest._id;
            expect(actualSpiffRuleId).toEqual(expectedSpiffRuleId);

        });

        it('throws if SpiffId is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    null,
                    dummy.serialNumber,
                    dummy.description,
                    dummy.multipleRequired
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'spiffId required');
        });

        it('sets spiffId', () => {
            /*
             arrange
             */
            const expectedSpiffId = dummy.spiffId;

            /*
             act
             */
            const objectUnderTest =
                new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    expectedSpiffId,
                    dummy.serialNumber,
                    dummy.description,
                    dummy.multipleRequired
                );

            /*
             assert
             */
            const actualSpiffId =
                objectUnderTest.spiffId;

            expect(actualSpiffId).toEqual(expectedSpiffId);

        });

        it('throws if serialNumber is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    dummy.spiffId,
                    null,
                    dummy.description,
                    dummy.multipleRequired
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'serialNumber required');
        });

        it('sets serialNumber', () => {
            /*
             arrange
             */
            const expectedSerialNumber = dummy.serialNumber;

            /*
             act
             */
            const objectUnderTest =
                new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    dummy.spiffId,
                    expectedSerialNumber,
                    dummy.description,
                    dummy.multipleRequired
                );

            /*
             assert
             */
            const actualSerialNumber =
                objectUnderTest.serialNumber;

            expect(actualSerialNumber).toEqual(expectedSerialNumber);

        });

        it('throws if description is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    dummy.spiffId,
                    dummy.serialNumber,
                    null,
                    dummy.multipleRequired
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'description required');
        });

        it('sets description', () => {
            /*
             arrange
             */
            const expectedDescription = dummy.description;

            /*
             act
             */
            const objectUnderTest =
                new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    dummy.spiffId,
                    dummy.serialNumber,
                    expectedDescription,
                    dummy.multipleRequired
                );

            /*
             assert
             */
            const actualDescription =
                objectUnderTest.description;

            expect(actualDescription).toEqual(expectedDescription);

        });

        it('throws if multipleRequired is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    dummy.spiffId,
                    dummy.serialNumber,
                    dummy.description,
                    null
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'multipleRequired required');
        });

        it('sets multipleRequired', () => {
            /*
             arrange
             */
            const expectedMultipleRequired = dummy.multipleRequired;

            /*
             act
             */
            const objectUnderTest =
                new SpiffRuleDataWebDto(
                    dummy.spiffRuleId,
                    dummy.spiffId,
                    dummy.serialNumber,
                    dummy.description,
                    expectedMultipleRequired
                );

            /*
             assert
             */
            const actualMultipleRequired =
                objectUnderTest.multipleRequired;

            expect(actualMultipleRequired).toEqual(expectedMultipleRequired);

        });

    });

});
