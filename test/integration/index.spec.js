import SpiffMasterDataServiceSdk,{
    SpiffMasterDataWebDto,
    SpiffRuleDataWebDto,
    ProductSaleRegistrationRequestWebDto,
    ProductSaleRegistrationResponseWebDto
} from  '../../src/index';
import  SpiffMasterDataWebView from '../../src/SpiffMasterDataWebView';
import  SpiffRuleDataWebView from '../../src/SpiffRuleDataWebView';
import  factory from './factory';
import config from './config';
import dummy from '../dummy';

/*
tests
 */

describe('Index module', () => {


    describe('default export', () => {
        it('should be SpiffMasterDataServiceSdk constructor', () => {
            /*
             act
             */
            const objectUnderTest =
                new SpiffMasterDataServiceSdk(
                    config.SpiffMasterDataServiceSdkConfig
                );

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(SpiffMasterDataServiceSdk));

        });
    });

    describe('saveMasterData method', () => {
        it('should return SpiffMasterDataWebView', (done) => {
            /*
             arrange
             */
            const objectUnderTest =
                new SpiffMasterDataServiceSdk(
                    config.SpiffMasterDataServiceSdkConfig
                );

            /*
             act
             */
            const actualPromise =
                objectUnderTest
                    .saveMasterData(
                        new SpiffMasterDataWebDto(
                            dummy.id,
                            dummy.name,
                            dummy.brand,
                            dummy.startDate,
                            dummy.endDate,
                            dummy.gracePeriod,
                            dummy.amount,
                            dummy.status
                        ),
                        factory.constructValidAppAccessToken()
                    );


            /*
            assert
            */
            actualPromise
                    .then(
                        (returnedSpiffMasterDataWebView) => {
                            expect(returnedSpiffMasterDataWebView).toBeTruthy();
                            done();
                        }
                    )
                    .catch(error=> done.fail(JSON.stringify(error)));

        }, 20000);
    });

    describe('listMasterData method', () => {
        it('should return more than 1 result', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new SpiffMasterDataServiceSdk(
                        config.SpiffMasterDataServiceSdkConfig
                    );

                /*
                 act
                 */
                const SpiffMasterDataWebViewsPromise =
                    objectUnderTest
                        .listMasterData(
                            factory.constructValidAppAccessToken()
                        );

                /*
                 assert
                 */
                SpiffMasterDataWebViewsPromise
                    .then((SpiffMasterDataWebView) => {
                        expect(SpiffMasterDataWebView.length>=1).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000);
    });

    describe('updateMasterData method', () => {
            it('should updateMasterData', (done) => {
                /*
                 arrange
                 */

                const objectUnderTest =
                    new SpiffMasterDataServiceSdk(
                        config.SpiffMasterDataServiceSdkConfig
                    );

                let spiffMasterDataWebDtoObj = new SpiffMasterDataWebDto(
                    dummy.id,
                    dummy.name,
                    dummy.brand,
                    dummy.startDate,
                    dummy.endDate,
                    dummy.gracePeriod,
                    dummy.amount,
                    dummy.status
                );

                let spiffMasterDataWebDto =  JSON.stringify(spiffMasterDataWebDtoObj);

                /*
                 act
                 */
                const spiffMasterDataWebViewsPromise =
                    objectUnderTest
                        .updateMasterData(
                            dummy.id,
                            spiffMasterDataWebDto,
                            factory.constructValidAppAccessToken()
                        );


                spiffMasterDataWebViewsPromise
                    .then(()=>
                        done()
                    ).catch(error=> done.fail(JSON.stringify(error)));

            }, 20000);
    });

    describe('saveRuleData method', () => {
        it('should return SpiffRuleDataWebView', (done) => {
            /*
             arrange
             */
            const objectUnderTest =
                new SpiffMasterDataServiceSdk(
                    config.SpiffMasterDataServiceSdkConfig
                );

            const saveMasterDataPromise =
                objectUnderTest
                    .saveMasterData(
                        new SpiffMasterDataWebDto(
                            dummy.id,
                            dummy.name,
                            dummy.brand,
                            dummy.startDate,
                            dummy.endDate,
                            dummy.gracePeriod,
                            dummy.amount,
                            dummy.status
                        ),
                        factory.constructValidAppAccessToken()
                    );


            /*
             act
             */
            const spiffRuleDataWebViewPromise =
                saveMasterDataPromise
                    .then(returnedSpiffMasterDataWebView =>
                        objectUnderTest
                            .saveRuleData(
                                new SpiffRuleDataWebDto(
                                    dummy.spiffRuleId,
                                    returnedSpiffMasterDataWebView.id,
                                    dummy.serialNumber,
                                    dummy.description,
                                    dummy.multipleRequired
                                ),
                                factory.constructValidAppAccessToken()
                            )
                    );

            /*
             assert
             */
            spiffRuleDataWebViewPromise
                .then((spiffRuleDataWebView) => {
                    expect(spiffRuleDataWebView).toBeTruthy();
                    done();
                })
                .catch(error => done.fail(JSON.stringify(error)));
        }, 20000);
    });

    describe('deleteRuleData method', () => {
        it('should delete the spiffRuleData', (done) => {
            /*
             arrange
             */
            const objectUnderTest =
                new SpiffMasterDataServiceSdk(
                    config.SpiffMasterDataServiceSdkConfig
                );

            /*
             act
             */
            const spiffRuleDataWebViewsPromise =
                objectUnderTest
                    .deleteRuleData(
                        dummy.spiffRuleId,
                        factory.constructValidAppAccessToken()
                    );

            /**
             assert
             */
            spiffRuleDataWebViewsPromise
                .then(()=>{
                    console.log("Rule Data deleted Successfully");
                    done();
                }).catch(error=> done.fail(JSON.stringify(error)));

        }, 20000);
    });

    describe('listRuleData method', () => {
        it('should return more than 1 result', (done) => {
            /*
             arrange
             */
            const objectUnderTest =
                new SpiffMasterDataServiceSdk(
                    config.SpiffMasterDataServiceSdkConfig
                );

            const saveMasterDataPromise =
                objectUnderTest
                    .saveMasterData(
                        new SpiffMasterDataWebDto(
                            dummy.id,
                            dummy.name,
                            dummy.brand,
                            dummy.startDate,
                            dummy.endDate,
                            dummy.gracePeriod,
                            dummy.amount,
                            dummy.status
                        ),
                        factory.constructValidAppAccessToken()
                    );

            /*
             act
             */
            const spiffRuleDataWebViewPromise =
                saveMasterDataPromise
                    .then(returnedSpiffMasterDataWebView =>
                        objectUnderTest
                            .saveRuleData(
                                new SpiffRuleDataWebDto(
                                    dummy.spiffRuleId,
                                    returnedSpiffMasterDataWebView.id,
                                    dummy.serialNumber,
                                    dummy.description,
                                    dummy.multipleRequired
                                ),
                                factory.constructValidAppAccessToken()
                            )
                    );

            /*
             act
             */
            const actualPromise =
                spiffRuleDataWebViewPromise
                    .then(spiffRuleDataWebView=>
                            objectUnderTest
                                .listRuleData(
                                    spiffRuleDataWebView.id,
                                    factory.constructValidAppAccessToken()
                                )
                    );

            /*
             assert
             */
            actualPromise
                .then((SpiffRuleWebViews) => {
                    expect(SpiffRuleWebViews.length>=1).toBeTruthy();
                    done();
                })
                .catch(error=> done.fail(JSON.stringify(error)));

        }, 20000);
    });

    describe('deleteRuleData method', () => {
        it('should delete the spiffRuleData', (done) => {
            /*
             arrange
             */
            const objectUnderTest =
                new SpiffMasterDataServiceSdk(
                    config.SpiffMasterDataServiceSdkConfig
                );

            /*
             act
             */
            const spiffRuleDataWebViewsPromise =
                objectUnderTest
                    .deleteRuleData(
                        dummy.spiffRuleId,
                        factory.constructValidAppAccessToken()
                    );


            /**
             assert
             */
            spiffRuleDataWebViewsPromise
                .then(()=>{
                    console.log("Rule Data deleted Successfully");
                    done();
                }).catch(error=> done.fail(JSON.stringify(error)));

        }, 20000);
    });

    describe('updateRuleData method', () => {
        it('should post the spiffRuleId', (done) => {
            /*
             arrange
             */
            const objectUnderTest =
                new SpiffMasterDataServiceSdk(
                    config.SpiffMasterDataServiceSdkConfig
                );

            /*
            act
             */
            const saveMasterDataPromise =
                objectUnderTest
                    .saveMasterData(
                        new SpiffMasterDataWebDto(
                            dummy.id,
                            dummy.name,
                            dummy.brand,
                            dummy.startDate,
                            dummy.endDate,
                            dummy.gracePeriod,
                            dummy.amount,
                            dummy.status
                        ),
                        factory.constructValidAppAccessToken()
                    );

            const updateRuleDataResponsePromise =
                saveMasterDataPromise
                    .then(spiffMasterDataView =>
                        objectUnderTest
                            .saveRuleData(
                                new SpiffRuleDataWebDto(
                                    dummy.spiffRuleId,
                                    spiffMasterDataView.id,
                                    dummy.serialNumber,
                                    dummy.description,
                                    dummy.multipleRequired
                                ),
                                factory.constructValidAppAccessToken()
                            ).then(spiffRuleDataWebView =>
                                objectUnderTest
                                    .updateRuleData(
                                        spiffRuleDataWebView.id,
                                        new SpiffRuleDataWebDto(
                                            dummy.spiffRuleId,
                                            spiffMasterDataView.id,
                                            dummy.serialNumber,
                                            dummy.description,
                                            dummy.multipleRequired
                                        ),
                                        factory.constructValidAppAccessToken()
                                    )
                        )
                    );

            /**
             assert
             */
            updateRuleDataResponsePromise
                .then(()=>{
                    done();
                    }
                ).catch(error=> done.fail(JSON.stringify(error)))

        }, 20000);
    });

    describe('deleteRuleData method', () => {
        it('should delete the spiffRuleData', (done) => {
            /*
             arrange
             */
            const objectUnderTest =
                new SpiffMasterDataServiceSdk(
                    config.SpiffMasterDataServiceSdkConfig
                );

            /*
             act
             */
            const spiffRuleDataWebViewsPromise =
                objectUnderTest
                    .deleteRuleData(
                        dummy.spiffRuleId,
                        factory.constructValidAppAccessToken()
                    );

            /*
            assert
             */
            spiffRuleDataWebViewsPromise
                .then(()=>{
                    console.log("Rule Data deleted Successfully");
                    done();
                }).catch(error=> done.fail(JSON.stringify(error)));

        }, 20000);
    });

    describe('deleteMasterData method', () => {
        it('should post the MasterDataId', (done) => {
            /*
             arrange
             */
            const objectUnderTest =
                new SpiffMasterDataServiceSdk(
                    config.SpiffMasterDataServiceSdkConfig
                );

            /*
             act
             */
            const spiffMasterDataWebViewsPromise =
                objectUnderTest
                    .deleteMasterData(
                        dummy.id,
                        factory.constructValidAppAccessToken()
                    );

            /*
            assert
             */
            spiffMasterDataWebViewsPromise
                .then(() => done()
                ).catch(error=> done.fail(JSON.stringify(error)));

        }, 20000);
    });

    describe('spiffAmountSerialNumber method', () => {
        it('should return ProductSaleRegistrationResponseWebDto', (done) => {
            /*
             arrange
             */
            const objectUnderTest =
                new SpiffMasterDataServiceSdk(
                    config.SpiffMasterDataServiceSdkConfig
                );

            /*
             act
             */
            const spiffRuleDataWebViewPromise =
                objectUnderTest
                    .spiffAmountSerialNumber(
                        new ProductSaleRegistrationRequestWebDto(
                            factory.constructSimpleLineItems(),
                            factory.constructCompositeLineItems()
                        ),
                        factory.constructValidAppAccessToken()
                    );

            /*
             assert
             */
            spiffRuleDataWebViewPromise
                .then((ProductSaleRegistrationResponseWebDto) => {
                    expect(ProductSaleRegistrationResponseWebDto).toBeTruthy();
                    done();
                })
                .catch(error=> done.fail(JSON.stringify(error)));

        }, 20000);
    });
});