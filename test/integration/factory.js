import config from './config';
import jwt from 'jwt-simple';
import dummy from '../dummy';
import PartnerSaleSimpleLineItemWebDto from '../../src/PartnerSaleSimpleLineItemWebDto';
import PartnerSaleCompositeLineItemWebDto from '../../src/PartnerSaleCompositeLineItemWebDto';

export default {
    constructValidAppAccessToken,
    constructSimpleLineItems,
    constructCompositeLineItems
}

    function constructValidAppAccessToken():string {

        const tenMinutesInMilliseconds = 10000 * 60;

        const jwtPayload = {
            "type": 'app',
            "exp": Date.now() + tenMinutesInMilliseconds,
            "aud": dummy.url,
            "iss": dummy.url
        };

        return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
    }


    function constructSimpleLineItems():PartnerSaleSimpleLineItemWebDto[]{

        return [{
            serialNumber:dummy.serialNumber
        }];

    }

    function constructCompositeLineItems():PartnerSaleCompositeLineItemWebDto[]{
        return [{
            components:constructSimpleLineItems()
        }];
    }
