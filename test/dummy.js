import  SpiffMasterDataWebDto from '../src/SpiffMasterDataWebDto';
import SpiffRuleDataWebDto from '../src/SpiffRuleDataWebDto';

const  dummy={
    id:1,
    name:'sample',
    brand:'test',
    startDate:'10/14/2015',
    endDate:'10/23/2015',
    gracePeriod:45,
    amount:100,
    status:'active',
    spiffRuleId:1,
    spiffId:1,
    serialNumber:"Test",
    description:"TestDescription",
    multipleRequired:'true',
    url: 'https://dummy-url.com'
};

export  default dummy;