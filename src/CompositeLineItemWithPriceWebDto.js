import SimpleLineItemWithPriceWebDto from './SimpleLineItemWithPriceWebDto';
/**
 * @class{CompositeLineItemWithPriceWebDto}
 */

export  default class CompositeLineItemWithPriceWebDto {

    _components:SimpleLineItemWithPriceWebDto[];

    _price:number;

    _isSpiffEligible:boolean;

    /**
     * @param {SimpleLineItemWithPriceWebDto} components
     * @param {number} price
     * @param {boolean} isSpiffEligible
     */
    constructor(
        components:SimpleLineItemWithPriceWebDto[],
        price:number,
        isSpiffEligible:boolean
    ) {

        this._components = components;

        this._price = price;

        this._isSpiffEligible = isSpiffEligible;

    }

    /**
     * getters
     */
    get components():SimpleLineItemWithPriceWebDto[] {
        return this._components;
    }

    get price():number {
        return this._price;
    }

    get isSpiffEligible():boolean {
        return this._isSpiffEligible;
    }

    toJSON(){
        return {
            components: this._components,
            price:this._price,
            isSpiffEligible: this._isSpiffEligible
        }
    }

}
