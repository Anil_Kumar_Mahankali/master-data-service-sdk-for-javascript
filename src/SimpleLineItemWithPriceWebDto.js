/**
 * @class {SimpleLineItemWithPriceWebDto}
 */
export default class SimpleLineItemWithPriceWebDto {

    _serialNumber:string;

    _amount:number;

    _isSpiffEligible:boolean;


    /**
     * @param {string} serialNumber
     * @param {number} amount
     * @param {boolean} isSpiffEligible
     */
    constructor(
        serialNumber:string,
        amount:number,
        isSpiffEligible:boolean
    ) {
        this._serialNumber = serialNumber;

        this._amount = amount;

        this._isSpiffEligible = isSpiffEligible;

    }

    get serialNumber():string {
        return this._serialNumber;
    }

    get amount():number {
        return this._amount
    }

    get isSpiffEligible():boolean {
        return this._isSpiffEligible;
    }

    toJSON() {
        return {
            serialNumber:this._serialNumber,
            amount:this._amount,
            isSpiffEligible: this._isSpiffEligible
        }
    }
}

