import PartnerSaleSimpleLineItemWebDto from './PartnerSaleSimpleLineItemWebDto';
import PartnerSaleCompositeLineItemWebDto from './PartnerSaleCompositeLineItemWebDto';

/**
 * @class{ProductSaleRegistrationRequestWebDto}
 */
export  default class ProductSaleRegistrationRequestWebDto {

    _simpleLineItems:PartnerSaleSimpleLineItemWebDto[];

    _compositeLineItems:PartnerSaleCompositeLineItemWebDto[];

    constructor(
        simpleLineItems:PartnerSaleSimpleLineItemWebDto[],
        compositeLineItems:PartnerSaleCompositeLineItemWebDto[]
    ) {

        this._simpleLineItems = simpleLineItems;

        this._compositeLineItems = compositeLineItems;
    }

    get simpleSerialCode():PartnerSaleSimpleLineItemWebDto[] {
        return this._simpleLineItems;
    }

    get compositeSerialCode():PartnerSaleCompositeLineItemWebDto[] {
        return this._compositeLineItems;
    }

    toJSON(){
        return {
            simpleLineItems: this._simpleLineItems,
            compositeLineItems: this._compositeLineItems
        }
    }

}
